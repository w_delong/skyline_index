package com.skyline.common;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.skyline.index.SkylineForest;

public class SkylineIndexTestSuit {
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	private static DecimalFormat df3 = new DecimalFormat("#.###");
	
	private SkylineIndex[] skis;
	private HashMap<SkylineIndex, List<SkylineIndexTestRun>> skiTestRunsMap;

	private List<DataNode> dataSet;
	private List<QueryNode> randomTestQueryNodes;
	
	private long numberOfDataNodes;
	private long numberOfDimentions;
	
	private final int NUMBER_OF_QUERY_NODES_PER_TEST = 10;
	
	public SkylineIndexTestSuit(List<DataNode> dataSet, SkylineIndex... skis) {
		this.skis = skis;
		this.dataSet = dataSet;

		this.randomTestQueryNodes = generateRandomTestQueryNodes(NUMBER_OF_QUERY_NODES_PER_TEST, dataSet);
		this.skiTestRunsMap = new HashMap<SkylineIndex, List<SkylineIndexTestRun>>();

		numberOfDataNodes = dataSet.size();
		numberOfDimentions = dataSet.get(0).getNumberOfAttributes();
	}

	public void runTests() {
		for (SkylineIndex ski: skis) {
			List<SkylineIndexTestRun> skiTestRuns = generateTestRunResults(ski);
			skiTestRunsMap.put(ski, skiTestRuns);
		}
	}

	public 	List<SkylineIndexTestRun> generateTestRunResults(SkylineIndex ski) {
		List<SkylineIndexTestRun> testRuns = new ArrayList<SkylineIndexTestRun>();

		for (QueryNode qn: randomTestQueryNodes) {
			SkylineIndexTestRun testRun = new SkylineIndexTestRun(ski);
			
			testRun.buildIndex(dataSet);
			testRun.searchIndex(qn);
			
			testRuns.add(testRun);
		}

		return testRuns;
	}
	
	private List<QueryNode> generateRandomTestQueryNodes(int numberOfNodes, List<DataNode> dataSet) {
		List<QueryNode> randonQueryNodes = new ArrayList<QueryNode>(numberOfNodes);

		for (int i = 0; i < numberOfNodes; i++) {
			randonQueryNodes.add(generateRandonQueryNode(dataSet));
		}

		return randonQueryNodes;
	}

	private QueryNode generateRandonQueryNode(List<DataNode> dataSet) {
		DataNode randomDataNode = getRandomDataNode(dataSet);
		Map<String, String> randomData = randomDataNode.data;
		Map<String, Integer> randomPreferences = getRandomPreferences(randomDataNode);
		
		QueryNode randomQueryNode = new QueryNode(randomData, randomPreferences);
	
		return randomQueryNode;
	}

	private Map<String, Integer> getRandomPreferences(DataNode randomDataNode) {
		Map<String, Integer> randomPreferences = new HashMap<String, Integer>();
		
		for (String attribute: randomDataNode.getAttributes()) {
			int randomPreferenceWeight = new Random().nextInt(randomDataNode.getNumberOfAttributes() + 1);
			randomPreferences.put(attribute, randomPreferenceWeight);
		}
		
		return randomPreferences;
	}

	private DataNode getRandomDataNode(List<DataNode> dataSet2) {
		int randomIndex = new Random().nextInt(dataSet.size());
		DataNode randomDataNode = dataSet.get(randomIndex); 
		
		return randomDataNode;
	}

	public String toString() {
		String out = "";
		
		for (SkylineIndex ski: skis) {
			String skiName = ski.getName();
			List<SkylineIndexTestRun> skiTestRuns = skiTestRunsMap.get(ski);

			out += generateSkiTestRunsToString(skiName, skiTestRuns);
		}

		return out;
	}

	private String generateSkiTestRunsToString(String skiName, List<SkylineIndexTestRun> skiTestRuns) {
		String out = "";

		out += "Skyline Index: " + skiName + "\n";
		out += "Number of Data Nodes: " + numberOfDataNodes + "\n";
		out += "Number of Dimensions: " + numberOfDimentions + "\n";
		out += "Number of Test Runs: " + skiTestRuns.size() + "\n";
		out += "Build Time (milli sec):\n";
		out += "\tMin: " + df3.format(getMinBuildTime(skiTestRuns))  + "\n";
		out += "\tMax: " + df3.format(getMaxBuildTime(skiTestRuns))  + "\n";
		out += "\tAvg: " + df3.format(getAvgBuildTime(skiTestRuns))  + "\n";
		out += "Search Time (milli sec):\n";
		out += "\tMin: " + df3.format(getMinSearchTime(skiTestRuns))  + "\n";
		out += "\tMax: " + df3.format(getMaxSearchTime(skiTestRuns))  + "\n";
		out += "\tAvg: " + df3.format(getAvgSearchTime(skiTestRuns))  + "\n";
		out += "Query Match Score:\n";
		out += "\tMin: " + df3.format(getMinQueryMatchScore(skiTestRuns))  + "\n";
		out += "\tMax: " + df3.format(getMaxQueryMatchScore(skiTestRuns))  + "\n";
		out += "\tAvg: " + df3.format(getAvgQueryMatchScore(skiTestRuns))  + "\n";
		out += "\n";
		
		return out;
	}

	private double getMinBuildTime(List<SkylineIndexTestRun> testRuns) {
		double minBuildTime = Integer.MAX_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			minBuildTime = Math.min(minBuildTime, tr.buildTime);
		}
		
		return minBuildTime;
	}
	
	private double getMaxBuildTime(List<SkylineIndexTestRun> testRuns) {
		double maxBuildTime = Integer.MIN_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			maxBuildTime = Math.max(maxBuildTime, tr.buildTime);
		}
		
		return maxBuildTime;
	}
	
	private double getAvgBuildTime(List<SkylineIndexTestRun> testRuns) {
		double avgBuildTime = 0.0;
		
		for (SkylineIndexTestRun tr: testRuns) {
			avgBuildTime += tr.buildTime;
		}
		
		avgBuildTime /= testRuns.size();
		
		return avgBuildTime;
	}
	
	private double getMinSearchTime(List<SkylineIndexTestRun> testRuns) {
		double minSearchTime = Integer.MAX_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			minSearchTime = Math.min(minSearchTime, tr.searchTime);
		}
		
		return minSearchTime;
	}
	
	private double getMaxSearchTime(List<SkylineIndexTestRun> testRuns) {
		double maxSearchTime = Integer.MIN_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			maxSearchTime = Math.max(maxSearchTime, tr.searchTime);
		}
		
		return maxSearchTime;
	}
	
	private double getAvgSearchTime(List<SkylineIndexTestRun> testRuns) {
		double avgSearchTime = 0.0;
		
		for (SkylineIndexTestRun tr: testRuns) {
			avgSearchTime += tr.searchTime;
		}
		
		avgSearchTime /= testRuns.size();
		
		return avgSearchTime;
	}
	
	private double getMinQueryMatchScore(List<SkylineIndexTestRun> testRuns) {
		double minQueryMatchScore = Integer.MAX_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			minQueryMatchScore = Math.min(minQueryMatchScore, tr.queryMatchScore);
		}
		
		return minQueryMatchScore;
	}
	
	private double getMaxQueryMatchScore(List<SkylineIndexTestRun> testRuns) {
		double maxQueryMatchScore = Integer.MIN_VALUE;
		
		for (SkylineIndexTestRun tr: testRuns) {
			maxQueryMatchScore = Math.max(maxQueryMatchScore, tr.queryMatchScore);
		}
		
		return maxQueryMatchScore;
	}
	
	private double getAvgQueryMatchScore(List<SkylineIndexTestRun> testRuns) {
		double avgQueryMatchScore = 0.0;
		
		for (SkylineIndexTestRun tr: testRuns) {
			avgQueryMatchScore += tr.queryMatchScore;
		}
		
		avgQueryMatchScore /= testRuns.size();
		
		return avgQueryMatchScore;
	}
}
