package com.skyline.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DataNode {
	Map<String, String> data;
	
	public DataNode() {
		data = new LinkedHashMap<String, String>(); 
	}
	
	public DataNode(Map<String, String> data) {
		this.data = data; 
	}
	
	
	public void addNewDataPair(String attribute, String value) {
		data.put(attribute, value);
	}
	
	public String getDataForAttribute(String attribute) {
		String dataValue = data.get(attribute);
		
		return dataValue;
	}
	
	public Set<String> getAttributes() {
		return data.keySet();
	}
	
	public Map<String, String> getData() {
		return data;
	}
	
	public int getNumberOfAttributes() {
		return getAttributes().size();
	}
	
	@Override
	public String toString() {
		ArrayList<String> strList = new ArrayList<String>(); 
		
		for (String key: data.keySet())
			strList.add(key + "=" + data.get(key));			

		String toString = "DataNode [" + String.join(", ", strList) + "]"; 
		
		return toString;
	}
}
