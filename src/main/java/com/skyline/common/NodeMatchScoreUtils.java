package com.skyline.common;

import java.util.List;

public class NodeMatchScoreUtils {
	
	public static double getQueryScore(QueryNode queryNode, List<DataNode> dataNodes) {
		
		double queryScore = 0.0;
		
		for (DataNode dn: dataNodes) {
			double nodeMatchScore = getNodeMatchScore(dn, queryNode);
			queryScore += nodeMatchScore; 
		}
		
		if (queryScore != 0.0)
			queryScore /= dataNodes.size(); 
		
		return queryScore;
	}
	
	private static double getNodeMatchScore(DataNode dn,QueryNode queryNode) {
		
		double nodeMatchScore = 0.0;
		
		for (String attribute: queryNode.data.keySet()) {
			String dataNodeValue = dn.getDataForAttribute(attribute);
			String queryNodeValue = queryNode.getDataForAttribute(attribute);
			
			if (dataNodeValue == null || queryNodeValue == null) {
				continue;
			}
			
			if (dataNodeValue.equals(queryNodeValue)) {
				nodeMatchScore += queryNode.getPreferenceWeightForAttribute(attribute);
			}
		}
		
		if (nodeMatchScore != 0.0)
			nodeMatchScore /= queryNode.getMaxMatchScore();
		
		return nodeMatchScore; 
	}
	
}
