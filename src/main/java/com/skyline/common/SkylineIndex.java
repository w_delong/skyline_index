package com.skyline.common;

import java.util.List;

public interface SkylineIndex {
	void build(List<DataNode> dataNodes);
	List<DataNode> search(QueryNode queryNode);
	String getName();
}
