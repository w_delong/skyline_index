package com.skyline.common;

import java.util.HashMap;
import java.util.Map;

public class QueryNode extends DataNode {
	private Map<String, Integer> preferences; 
	
	public QueryNode() {
		super();
		preferences = new HashMap<String, Integer>();
	}
	
	public QueryNode(Map<String, String> data) {
		super(data);
		preferences = new HashMap<String, Integer>();
	}
	
	public QueryNode(Map<String, String> data, Map<String, Integer> preferences) {
		super(data);
		this.preferences = preferences; 
	}

	public Map<String, Integer> getPreferences() {
		return preferences;
	}

	public void setPreferences(Map<String, Integer> preferences) {
		this.preferences = preferences;
	}
	
	public void setPreference(String attribute, int preferenceWeight) {
		if (preferences == null) {
			preferences = new HashMap<String, Integer>();
		}

		preferences.put(attribute, preferenceWeight);
	}
	
	public int getMaxMatchScore() {
		int maxMatchScore = 0;
		
		for (String preference: data.keySet()) {
			maxMatchScore += getPreferenceWeightForAttribute(preference);
		}
		
		return maxMatchScore;
	}
	
	public int getPreferenceWeightForAttribute(String attribute) {
		if (preferences == null) {
			return 1;
		}
		
		return preferences.getOrDefault(attribute, 1);
	}
	
	public String toString() {
		String toString = super.toString() + "-" + getPreferenceToString();
		
		return toString;
	}

	private String getPreferenceToString() {
		String preferenceToString = "preferences=";
		
		for (String attribute: data.keySet()) {
			preferences.put(attribute, getPreferenceWeightForAttribute(attribute));
		}
		preferenceToString += preferences;
		
		return preferenceToString;
	}
}
