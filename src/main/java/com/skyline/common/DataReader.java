package com.skyline.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataReader {
	
	Scanner scanner;
	
	public DataReader(String dataFilePath) {
		this.scanner = createDataScanner(dataFilePath);
	}

	public ArrayList<DataNode> generateDataNodes() {
		ArrayList<DataNode> dataNodes = readDataNodesFromFile();
		
        return dataNodes;
	}
	
	public void setDataFilePath(String dataFilePath) {
		this.scanner = createDataScanner(dataFilePath);
	}
	
	private Scanner createDataScanner(String dataFilePath) {
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new File(dataFilePath));
			scanner.useDelimiter("\n");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return scanner;
	}
	
	private ArrayList<DataNode> readDataNodesFromFile() {
		ArrayList<DataNode> dataNodes = new ArrayList<DataNode>();
		
		ObjectMapper objectMapper = new ObjectMapper();
		
        while(scanner.hasNext())
        	dataNodes.add(generateNode(objectMapper, scanner.next()));
        
        scanner.close();
		
		return dataNodes;
	}

	private DataNode generateNode(ObjectMapper objectMapper, String json) {
		HashMap<String, String> data = new HashMap<String, String>();
		
		try
		{
			data = objectMapper.readValue(json, new TypeReference<Map<String, String>>(){});
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return new DataNode(data);
	}
}
