package com.skyline.common;

import java.util.List;

public class SkylineIndexTestRun {
	private SkylineIndex ski;
	double buildTime;
	double searchTime;
	QueryNode queryNode;
	double queryMatchScore;

	public SkylineIndexTestRun(SkylineIndex ski) {
		this.ski = ski;
	}

	public void buildIndex(List<DataNode> dataNodes) {
		long startTime = System.nanoTime(); 
		ski.build(dataNodes);
		buildTime = getMilli(System.nanoTime() - startTime);
	}

	public void searchIndex(QueryNode queryNode) {
		this.queryNode = queryNode;
		
		long startTime = System.nanoTime(); 
		List<DataNode> searchResults = ski.search(queryNode);
		searchTime = getMilli(System.nanoTime() - startTime);
		
		queryMatchScore = NodeMatchScoreUtils.getQueryScore(queryNode, searchResults.subList(0, 10));
	}
	
	private double getMilli(long nanoSecs) {
		return (double)nanoSecs / 1000000;
	}
}
