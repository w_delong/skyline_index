package com.skyline.index;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.skyline.common.DataNode;
import com.skyline.common.QueryNode;
import com.skyline.common.SkylineIndex;

public class SkylineForest implements SkylineIndex {
	HashMap<String, AttributeTree> forest;
	
	public SkylineForest() {
		forest = new HashMap<String, AttributeTree>();
	}
	
	public SkylineForest(List<DataNode> dataNodes) {
		forest = new HashMap<String, AttributeTree>();
		add(dataNodes);
	}
	
	public void add(DataNode dataNodes) {
		ensureTreesForeachAttribute(dataNodes.getAttributes());
		for (AttributeTree tree: getAttributeTrees())
			tree.add(dataNodes);
	}
	
	public void add(List<DataNode> dataNodes) {
		for (DataNode dn: dataNodes)
			add(dn);
	}
	
	public List<DataNode> getSkyline(QueryNode queryNode) {
		RankedSkylineBuilder rkSkylineBuilder = new RankedSkylineBuilder(queryNode);
		populateBuilder(rkSkylineBuilder);

		return rkSkylineBuilder.getResult();
	}
	
//	public List<DataNode> getSkyline(QueryNode queryNode) {
//		ExecutorService executor = Executors.newCachedThreadPool();
//		
//		
//		
//		//executor.in
//	}
	
	public int size() {
		return forest.size();
	}
	
	public Collection<AttributeTree> getAttributeTrees() {
		return forest.values();
	}
	
	public AttributeTree getAttributeTree(String attributeTreeName) {
		return forest.get(attributeTreeName);
	}

	private void populateBuilder(RankedSkylineBuilder rkSkylineBuilder) {
		QueryNode queryNode = rkSkylineBuilder.getQueryNode();

		for (String attribute: queryNode.getAttributes()) {
			ArrayList<DataNode> matchingDataNodes = getMatchingDataNodesFromAttributeTree(attribute, queryNode.getDataForAttribute(attribute));
			rkSkylineBuilder.mapDataNodes(matchingDataNodes, queryNode.getPreferenceWeightForAttribute(attribute));
		}
	}

	private ArrayList<DataNode> getMatchingDataNodesFromAttributeTree(String attributeTree, String branch) {
		ArrayList<DataNode> dataNodes = new ArrayList<DataNode>();

		if (forest.containsKey(attributeTree))
			dataNodes = forest.get(attributeTree).getDataNodesForBranch(branch);
		
		return dataNodes;
	}

	private void ensureTreesForeachAttribute(Set<String> attributes) {
		for (String attribute: attributes) {
			if (!forest.containsKey(attribute))
				forest.put(attribute, new AttributeTree(attribute));
		}
	}
	
	public String toString() {
		String toString = "Forest contains " + forest.size() + " trees:\n";
		
		for (AttributeTree tree: forest.values())
			toString += "\t" + tree.toString() + "\n";
		
		return toString;
	}

	@Override
	public void build(List<DataNode> dataNodes) {
		forest = new HashMap<String, AttributeTree>();
		add(dataNodes);
	}

	@Override
	public List<DataNode> search(QueryNode queryNode) {
		return getSkyline(queryNode);
	}

	@Override
	public String getName() {
		return "Skyline Forest";
	}
}
