package com.skyline.index;

import java.util.ArrayList;
import java.util.HashMap;

import com.skyline.common.DataNode;

public class AttributeTree {
	String name;
	private HashMap<String, ArrayList<DataNode>> branches;
	
	public AttributeTree(String name) {
		this.name = name;
		setBranches(new HashMap<String, ArrayList<DataNode>>());
	}
	
	public AttributeTree(String name, ArrayList<DataNode> dataNodes) {
		this.name = name;
		setBranches(new HashMap<String, ArrayList<DataNode>>());
		add(dataNodes);
	}
	
	public void add(DataNode dataNode) {
		String branchValue = dataNode.getDataForAttribute(name);
		ArrayList<DataNode> branch = getDataNodesForBranch(branchValue);
		branch.add(dataNode);
	}
	
	public void add(ArrayList<DataNode> dataNodes) {
		for (DataNode dn: dataNodes)
			add(dn);
	}
	
	public ArrayList<DataNode> getDataNodesForBranch(String branchKey) {
		if (!getBranches().containsKey(branchKey)) {
			getBranches().put(branchKey, new ArrayList<DataNode>());
		}
		
		return getBranches().get(branchKey);
	}
	
	public ArrayList<DataNode> searchTree(String searchAttribute) {
		return getDataNodesForBranch(searchAttribute);
	}
	
	public int size() {
		return branches.size();
	}
	
	private HashMap<String, ArrayList<DataNode>> getBranches() {
		return branches;
	}

	private void setBranches(HashMap<String, ArrayList<DataNode>> branches) {
		this.branches = branches;
	}

	public String toString() {
		String toString = name + "-tree contains " + getBranches().size() + " branches:\n";
		
		for (String key: getBranches().keySet()) {
			ArrayList<DataNode> branch = getBranches().get(key);
			toString += "\t\t" + key + "-branch contains " + branch.size() + " nodes:\n";
			
			for (DataNode dn: branch)
				toString += "\t\t\t" + dn.toString() + "\n";
		}
		
		return toString;
	}	
}
