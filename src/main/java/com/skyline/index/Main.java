package com.skyline.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;
import com.skyline.common.SkylineIndex;
import com.skyline.common.SkylineIndexTestSuit;

public class Main {
	public static void main(String[] args) {
		DataReader dr = new DataReader("src/main/java/com/skyline/data_sets/data.txt");
		ArrayList<DataNode> dataNodes = dr.generateDataNodes();
		
		SkylineIndex[] skis = {new SkylineForest(), new SkylineForest(), new SkylineForest()};

		SkylineIndexTestSuit ts1 = new SkylineIndexTestSuit(dataNodes, skis);
				
		ts1.runTests();
		println(ts1);
	}
	
	private static void println(Object o) {
		System.out.println(o);
	}
	
	private static void print(Object o) {
		System.out.print(o);
	}
	
	private static void print(ArrayList<?> arr) {
		for (Object o: arr)
			println(o);
	}
}
