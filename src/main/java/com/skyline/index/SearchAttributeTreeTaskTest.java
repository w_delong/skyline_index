package com.skyline.index;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;

public class SearchAttributeTreeTaskTest {
	
	ArrayList<DataNode> dataNodes;
	DataReader dr;
	SkylineForest skf;
	QueryNode queryNode;

	@Before
	public void setUp() throws Exception {
		//dr = new DataReader("src/main/java/com/skyline/data_sets/data.txt");
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
		dataNodes = dr.generateDataNodes();
		skf = new SkylineForest(dataNodes);
		
		System.out.println(skf);
		
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$");
		queryNode.addNewDataPair("distance", "close");
//		queryNode.addNewDataPair("color", "red");
//		queryNode.addNewDataPair("type", "car");
//		queryNode.setPreference("color", 1);
//		queryNode.setPreference("type", 1);
	}

	@Test
	public void test() throws InterruptedException, ExecutionException {
		ExecutorService threadPool = Executors.newCachedThreadPool();
		ExecutorCompletionService<Void> compService = new ExecutorCompletionService<Void>(threadPool);

		RankedSkylineBuilder rkSkylineBuilder = new RankedSkylineBuilder(queryNode);
		
		for (String attribute: queryNode.getAttributes()) {
			AttributeTree aTree = skf.getAttributeTree(attribute);
			String searchAttribute = queryNode.getDataForAttribute(attribute);
			
			compService.submit(new SearchAttributeTreeTask(aTree, searchAttribute, rkSkylineBuilder));
		}
		
		threadPool.shutdown();

		while (!threadPool.isTerminated()) {
			//System.out.println("Waiting...");
		}
		
		System.out.println(String.format("Query Node: %s\n", queryNode));
		
		System.out.println("Skyline:");
		for (DataNode dn: skf.getSkyline(queryNode)) {
			System.out.println(dn);
		}
	}

}
