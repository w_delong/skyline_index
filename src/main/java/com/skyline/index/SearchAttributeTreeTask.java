package com.skyline.index;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import com.skyline.common.DataNode;

public class SearchAttributeTreeTask implements Callable<Void> {
	String searchAttribute;
	AttributeTree attributeTree;
	RankedSkylineBuilder rkSkylineBuilder;
	
	public SearchAttributeTreeTask(AttributeTree attributeTree, String searchAttribute, RankedSkylineBuilder rkSkylineBuilder) {
		this.searchAttribute = searchAttribute;
		this.attributeTree = attributeTree;
		this.rkSkylineBuilder = rkSkylineBuilder;
	}

	public void run() {
//		ArrayList<DataNode> matchingDataNodes = getMatchingDataNodesFromAttributeTree(searchAttribute, queryNode.getDataForAttribute(searchAttribute));
//		rkSkylineBuilder.mapDataNodes(matchingDataNodes, queryNode.getPreferenceWeightForAttribute(searchAttribute));
	}

	@Override
	public Void call() throws Exception {
		ArrayList<DataNode> dataNodes = attributeTree.searchTree(searchAttribute);
		rkSkylineBuilder.mapDataNodes(dataNodes, searchAttribute);
		
		return null;
	}
}
