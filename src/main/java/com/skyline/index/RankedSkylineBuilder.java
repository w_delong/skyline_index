package com.skyline.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.skyline.common.DataNode;
import com.skyline.common.QueryNode;

public class RankedSkylineBuilder {
	private QueryNode queryNode;
	private HashMap<Integer, Set<DataNode>> rankedBuckets;
	private HashMap<DataNode, Integer> dataNodeScores;
	private int lowestRank;
	
	public RankedSkylineBuilder(QueryNode queryNode) {
		this.setQueryNode(queryNode); 
		this.lowestRank = queryNode.getMaxMatchScore();
		this.rankedBuckets = new HashMap<Integer, Set<DataNode>>();
		this.dataNodeScores = new HashMap<DataNode, Integer>();
	}
	
	public void mapDataNodes(List<DataNode> matchingNodes, int matchScore) {
		for (DataNode dn: matchingNodes) {
			removeFromRankedBucket(dn);
			updateDataNodeScore(dn, matchScore);
			addToRankedBucket(dn);
		}
	}
	
	public void mapDataNodes(List<DataNode> matchingNodes, String searchAttribute) {
		int matchScore = queryNode.getPreferenceWeightForAttribute(searchAttribute);
		
		for (DataNode dn: matchingNodes) {
			removeFromRankedBucket(dn);
			updateDataNodeScore(dn, matchScore);
			addToRankedBucket(dn);
		}
	}
	
	public ArrayList<DataNode> getResult() {
		ArrayList<DataNode> rankedNodes = new ArrayList<DataNode>();
		
		for (Set<DataNode> bucketOfNodes: rankedBuckets.values()) {
			rankedNodes.addAll(bucketOfNodes);
		}
		
		return rankedNodes;
	}
	
	public QueryNode getQueryNode() {
		return queryNode;
	}

	public void setQueryNode(QueryNode queryNode) {
		this.queryNode = queryNode;
	}
	
	private void updateDataNodeScore(DataNode dn, int matchScore) {
		int newScore = getDataNodeScore(dn) + matchScore;
		dataNodeScores.put(dn, newScore);
	}

	private void removeFromRankedBucket(DataNode dn) {
		Set<DataNode> dnBucket = rankedBuckets.get(getDataNodeRank(dn));
		
		if (dnBucket == null) {
			return;
		}
		
		dnBucket.remove(dn);
	}
	
	private void addToRankedBucket(DataNode dn) {
		int dnRank = getDataNodeRank(dn);
	
		if (!rankedBuckets.containsKey(dnRank)) {
			rankedBuckets.put(dnRank, new HashSet<DataNode>());
		}
	
		Set<DataNode> dnBucket = rankedBuckets.get(getDataNodeRank(dn));
		dnBucket.add(dn);
	}

	private int getDataNodeRank(DataNode dn) {
		return Math.max(0, lowestRank - getDataNodeScore(dn));
	}
	
	private int getDataNodeScore(DataNode dn) {
		return dataNodeScores.getOrDefault(dn, 0);
	}
	
	public String toString() {
		String toString = "Builder Query Node: " + queryNode + "\n\n";
		toString += "Builder contains " + rankedBuckets.size() + " ranked buckets:\n";
		
		for (Integer i: rankedBuckets.keySet()) {
			Set<DataNode> bucketOfNodes = rankedBuckets.get(i);
			toString += "\tbucket " + i + ":\n";
			for (DataNode dn: bucketOfNodes) {
				
				toString += "\t\tscore " + getDataNodeScore(dn) + ": " + dn + "\n";
			}
		}
		
		return toString;
	}
}
