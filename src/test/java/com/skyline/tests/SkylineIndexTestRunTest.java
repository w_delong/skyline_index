package com.skyline.tests;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;
import com.skyline.common.SkylineIndexTestRun;
import com.skyline.index.SkylineForest;

public class SkylineIndexTestRunTest {
	
	ArrayList<DataNode> dataNodes;
	DataReader dr;
	SkylineForest skf;
	QueryNode queryNode;

	@Before
	public void setUp() throws Exception {
		//dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
		dr = new DataReader("src/main/java/com/skyline/data_sets/data.txt");
		dataNodes = dr.generateDataNodes();
		skf = new SkylineForest();
		
		//System.out.println(skf);
		
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$");
		queryNode.addNewDataPair("distance", "close");
		queryNode.addNewDataPair("color", "red");
		queryNode.addNewDataPair("type", "car");
		queryNode.setPreference("color", 1);
		queryNode.setPreference("type", 1);
	}

	@Test
	public void test() {
		SkylineIndexTestRun tSki = new SkylineIndexTestRun(skf);
//		
//		tSki.build(dataNodes);
//		tSki.search(queryNode);
	}

}
