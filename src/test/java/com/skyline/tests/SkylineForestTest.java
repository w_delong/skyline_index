package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;
import com.skyline.index.AttributeTree;
import com.skyline.index.SkylineForest;
import java.util.Collection;
import java.util.List;

public class SkylineForestTest {

	ArrayList<DataNode> dataNodes;
	DataReader dr;

	@Before
	public void setUp() throws Exception {
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
		dataNodes = dr.generateDataNodes();
	}
	
	@Test
	public void testAdd() {
		SkylineForest skf = new SkylineForest();
		
		for (DataNode dn: dataNodes)
			skf.add(dn);

		assertEquals(2, skf.size());
		for (AttributeTree at: skf.getAttributeTrees())
			assertEquals(2, at.size());
	}
	
	@Test
	public void testBulkAdd() {
		SkylineForest skf = new SkylineForest();
		skf.add(dataNodes);

		assertEquals(2, skf.size());
		for (AttributeTree at: skf.getAttributeTrees())
			assertEquals(2, at.size());
	}
	
	@Test
	public void testGetSkyline() {
		List<DataNode> dataNodes = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt").generateDataNodes();
		
		QueryNode queryNode = new QueryNode(dataNodes.get(0).getData());
		SkylineForest skf = new SkylineForest(dataNodes);
		
		List<DataNode> skylineNodes = skf.getSkyline(queryNode);

		assertNotEquals(dataNodes.size(), skylineNodes.size());
		assertEquals(skylineNodes.size(), 2);
		
		String actualSkylineNodes = skylineNodes.toString();
		String expectedSkylineNodes = "[DataNode [price=$$$, distance=close], "
									+ "DataNode [price=$$$, distance=far]]";
		
		assertEquals(expectedSkylineNodes, actualSkylineNodes);
	}
	
	@Test
	public void testGetSkylineWithPreferences() {
		List<DataNode> dataNodes = new DataReader("src/test/java/com/skyline/tests/data/test_data_2.txt").generateDataNodes();
		
		QueryNode queryNode = new QueryNode(dataNodes.get(0).getData());
		queryNode.setPreference("distance", 2);
		SkylineForest skf = new SkylineForest(dataNodes);
		
		List<DataNode> skylineNodes = skf.getSkyline(queryNode);

		String actualSkylineNodes = skylineNodes.toString();
		String expectedSkylineNodes = "[DataNode [price=$$$, distance=close], "
									+ "DataNode [price=$$, distance=close], "
									+ "DataNode [price=$$$, distance=far]]";
		
		assertEquals(expectedSkylineNodes, actualSkylineNodes);
	}
	
	@Test
	public void testToString() {
		SkylineForest skf = new SkylineForest(dataNodes);
		
		String expectedToString = "Forest contains 2 trees:\n"
				+ "\tdistance-tree contains 2 branches:\n"
				+ "\t\tfar-branch contains 2 nodes:\n"
				+ "\t\t\tDataNode [price=$$$, distance=far]\n"
				+ "\t\t\tDataNode [price=$$, distance=far]\n"
				+ "\t\tclose-branch contains 1 nodes:\n"
				+ "\t\t\tDataNode [price=$$$, distance=close]\n\n"
				+ "\tprice-tree contains 2 branches:\n"
				+ "\t\t$$-branch contains 1 nodes:\n"
				+ "\t\t\tDataNode [price=$$, distance=far]\n"
				+ "\t\t$$$-branch contains 2 nodes:\n"
				+ "\t\t\tDataNode [price=$$$, distance=close]\n"
				+ "\t\t\tDataNode [price=$$$, distance=far]\n\n";
		
		String actualToString = skf.toString();

		assertEquals(expectedToString, actualToString);
	}
}
