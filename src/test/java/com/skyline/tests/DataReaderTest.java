package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;

public class DataReaderTest {

	DataReader dr;
	
	@Before
	public void setUp() throws Exception {
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
	}

	@Test
	public void testGenerateDataNodes() {
		ArrayList<DataNode> dataNodes = dr.generateDataNodes();
		
		String expectedToString = "DataNode [price=$$$, distance=close]"
								+ "DataNode [price=$$$, distance=far]"
								+ "DataNode [price=$$, distance=far]"; 
				
		assertEquals(3, dataNodes.size());
		assertEquals(expectedToString, getToString(dataNodes));
	}
	
	@Test
	public void testSetDataFilePath() {
		dr.setDataFilePath("src/test/java/com/skyline/tests/data/test_data_2.txt");
		
		ArrayList<DataNode> dataNodes = dr.generateDataNodes();
		
		String expectedToString = "DataNode [price=$$$, distance=close]"
								+ "DataNode [price=$$$, distance=far]"
								+ "DataNode [price=$$, distance=far]"
								+ "DataNode [price=$$, distance=close]"
								+ "DataNode [price=$, distance=far]"; 
				
		assertEquals(5, dataNodes.size());
		assertEquals(expectedToString, getToString(dataNodes));
	}

	private String getToString(ArrayList<DataNode> dataNodes) {
		String toString = "";
		
		for (DataNode dn: dataNodes)
			toString += dn.toString();
		
		return toString;
	}

}
