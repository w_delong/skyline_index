package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.skyline.common.DataNode;

import org.junit.Before;

public class DataNodeTest  {
	
	DataNode dataNode;
	
	@Before
	public void setUp() throws Exception {
		dataNode = new DataNode();
		dataNode.addNewDataPair("price", "$");
		dataNode.addNewDataPair("distance", "close");
		dataNode.addNewDataPair("color", "red");
		dataNode.addNewDataPair("type", "car");
	}
	
	@Test
	public void testGetDataForAttribute() {
		String data = dataNode.getDataForAttribute("price");
		assertEquals("$", data);
		
		data = dataNode.getDataForAttribute("distance");
		assertEquals("close", data);
		
		data = dataNode.getDataForAttribute("color");
		assertEquals("red", data);
		
		data = dataNode.getDataForAttribute("type");
		assertEquals("car", data);
		
		data = dataNode.getDataForAttribute("error");
		assertEquals(null, data);
	}

	@Test
	public void testGetDataPairs() {
		Map<String, String> dataPairs = dataNode.getData();
		
		assertEquals(4, dataPairs.size());
		
		String expectedToString = "{price=$, distance=close, color=red, type=car}";
		String actualToString =  dataPairs.toString();
		
		assertEquals(expectedToString, actualToString);
	}
	
	@Test
	public void testToString() {
		String toString = dataNode.toString();	
		assertEquals("DataNode [price=$, distance=close, color=red, type=car]", toString);
	}
	
	
}
