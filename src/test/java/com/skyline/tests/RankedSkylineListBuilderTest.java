package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;
import com.skyline.index.RankedSkylineBuilder;
import com.skyline.index.SkylineForest;

public class RankedSkylineListBuilderTest {
	ArrayList<DataNode> dataNodes;
	DataReader dr;
	QueryNode queryNode;
	

	@Before
	public void setUp() throws Exception {
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_ranked_skyline_builder_1.txt");
		dataNodes = dr.generateDataNodes();
		
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$");
		queryNode.addNewDataPair("distance", "close");
		queryNode.addNewDataPair("color", "red");
	}

	@Test
	public void testMapDataNodes() {
		RankedSkylineBuilder rkSkylineBuilder;
		rkSkylineBuilder = new RankedSkylineBuilder(queryNode);
		
		rkSkylineBuilder.mapDataNodes(dataNodes, 1);
		rkSkylineBuilder.mapDataNodes(dataNodes.subList(0, 3), 1);
		rkSkylineBuilder.mapDataNodes(dataNodes.subList(0, 1), 1);
		
		List<DataNode> rankedDataNodes = rkSkylineBuilder.getResult();
		assertEquals(dataNodes.size(), rankedDataNodes.size());
	}
	
	@Test
	public void testToString() {
		RankedSkylineBuilder rkSkylineBuilder;
		rkSkylineBuilder = new RankedSkylineBuilder(queryNode);
		
		rkSkylineBuilder.mapDataNodes(dataNodes.subList(0, 1), 1);
		rkSkylineBuilder.mapDataNodes(dataNodes.subList(0, 2), 1);
		
		String expectedToString = "Builder Query Node: DataNode [price=$, distance=close, color=red]-preferences={distance=1, color=1, price=1}\n\n" 
								+ "Builder contains 2 ranked buckets:\n"
								+ "\tbucket 1:\n"
								+ "\t\tscore 2: DataNode [price=$, distance=close, color=red]\n"
								+ "\tbucket 2:\n"
								+ "\t\tscore 1: DataNode [price=$, distance=close, color=green]\n";
				
		String actualToString = rkSkylineBuilder.toString();

		assertEquals(expectedToString, actualToString);
	}
}
