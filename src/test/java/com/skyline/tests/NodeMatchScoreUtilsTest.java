package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.AssertTrue;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.common.QueryNode;
import com.skyline.common.NodeMatchScoreUtils;
import com.skyline.index.SkylineForest;

public class NodeMatchScoreUtilsTest {

	ArrayList<DataNode> dataNodes;
	DataReader dr;
	SkylineForest skf;
	QueryNode queryNode;

	@Before
	public void setUp() throws Exception {
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
		//dr = new DataReader("src/main/java/com/skyline/data_sets/data.txt");
		dataNodes = dr.generateDataNodes();
		skf = new SkylineForest(dataNodes);
	}

	@Test
	public void testPartialMatch() {
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$");
		queryNode.addNewDataPair("distance", "close");
		
		double expectedScore = 0.5;
		double actualScore = NodeMatchScoreUtils.getQueryScore(queryNode, skf.search(queryNode));

		assertEquals(expectedScore, actualScore, 0.000001);
	}
	
	@Test
	public void testFullMatch() {
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$$");
		queryNode.addNewDataPair("distance", "close");
		
		double expectedScore = 1.0;
		double actualScore = NodeMatchScoreUtils.getQueryScore(queryNode, skf.search(queryNode).subList(0, 1));

		assertEquals(expectedScore, actualScore, 0.000001);
	}
	
	@Test
	public void testNoMatch() {
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$");
		queryNode.addNewDataPair("distance", "very-close");
		
		double expectedScore = 0.0;
		double actualScore = NodeMatchScoreUtils.getQueryScore(queryNode, skf.search(queryNode));

		assertEquals(expectedScore, actualScore, 0.000001);
	}
	
	@Test
	public void testPreferencesMatch1() {
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$$");
		queryNode.addNewDataPair("distance", "close");
		queryNode.setPreference("price", 2);
		
		double expectedScore = 0.833;
		double actualScore = NodeMatchScoreUtils.getQueryScore(queryNode, skf.search(queryNode));

		assertEquals(expectedScore, actualScore, 0.001);
	}
	
	@Test
	public void testPreferencesMatch2() {
		queryNode = new QueryNode();
		queryNode.addNewDataPair("price", "$$$");
		queryNode.addNewDataPair("distance", "close");
		queryNode.setPreference("price", 3);
		queryNode.setPreference("distance", 2);
		
		double expectedScore = 0.80;
		double actualScore = NodeMatchScoreUtils.getQueryScore(queryNode, skf.search(queryNode));
	
		assertEquals(expectedScore, actualScore, 0.001);
	}
	
	private void printSkylineResults(List<DataNode> resutls) {
		System.out.println("Skyline:");
		for (DataNode dn: resutls) {
			System.out.println(dn);
		}
	}

}
