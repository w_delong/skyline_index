package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.constraints.AssertTrue;

import org.junit.Before;
import org.junit.Test;
import com.skyline.common.QueryNode;

public class QueryNodeTest {
	
	QueryNode queryNode;

	@Before
	public void setUp() throws Exception {
		queryNode = new QueryNode();
		
		for (Entry<String, String> dataPair: getBottledData().entrySet()) {
			queryNode.addNewDataPair(dataPair.getKey(), dataPair.getValue());
		}
	}
	
	@Test
	public void testCompoundConstructor() {
		queryNode = new QueryNode(getBottledData(), getBottledPreferences());
		
		Map<String, Integer> expectedPreferences = getBottledPreferences();
		Map<String, Integer> actualPreferences = queryNode.getPreferences();
		
		assertPreferencesLineUp(expectedPreferences, actualPreferences);
	}

	@Test
	public void testSetGetPreferences() {
		queryNode.setPreferences(getBottledPreferences());
		
		Map<String, Integer> expectedPreferences = getBottledPreferences();
		Map<String, Integer> actualPreferences = queryNode.getPreferences();
		
		assertPreferencesLineUp(expectedPreferences, actualPreferences);
	}

	@Test
	public void testDefaultGetMaxMatchScore() {
		queryNode = new QueryNode(getBottledData());
		
		int expectedDefaultSocre = queryNode.getAttributes().size(); // 1 for each attribute
		int actualDefaultScore = queryNode.getMaxMatchScore();
		
		assertEquals(expectedDefaultSocre, actualDefaultScore);
	}
	
	@Test
	public void testGetMaxMatchScore() {
		queryNode.setPreferences(getBottledPreferences());
		
		int expectedSocre = queryNode.getPreferences().values().stream().mapToInt(Integer::intValue).sum();
		int actualScore = queryNode.getMaxMatchScore();
		
		assertEquals(expectedSocre, actualScore);
	}
	
	@Test
	public void testGetPreferenceWeightForPreference() {
		Map<String, Integer> expectedPreferences = getBottledPreferences();
		queryNode.setPreferences(expectedPreferences);

		assertPreferenceWeightsLineUp(expectedPreferences, queryNode);
	}
	
	@Test
	public void testUnknownGetPreferenceWeightForAttribute() {
		int expectedUnknownPreference = 1;
		int actualUnknownPreference = queryNode.getPreferenceWeightForAttribute("something unknown");
		
		assertEquals(expectedUnknownPreference, actualUnknownPreference);
	}
	
	@Test
	public void testNullGetPreferenceWeightForAttribute() {
		QueryNode qn = new QueryNode(getBottledData(), null);

		Map<String, Integer> expectedNullPreferences = new HashMap<String, Integer>();
		qn.getAttributes().forEach((p) -> expectedNullPreferences.put(p, 1));
		
		assertPreferenceWeightsLineUp(expectedNullPreferences, qn);
	}
	
	@Test
	public void testSetPreference() {
		queryNode = new QueryNode(getBottledData());
		assertTrue(queryNode.getPreferences().isEmpty());
		
		queryNode.setPreference("price", 5);
		assertFalse(queryNode.getPreferences().isEmpty());
		assertEquals(1, queryNode.getPreferences().size());
		assertEquals(5, queryNode.getPreferenceWeightForAttribute("price"));
		
		queryNode.setPreference("price", 1);
		assertFalse(queryNode.getPreferences().isEmpty());
		assertEquals(1, queryNode.getPreferences().size());
		assertEquals(1, queryNode.getPreferenceWeightForAttribute("price"));
	}
	
	@Test
	public void testToString() {
		queryNode = new QueryNode(getBottledData());
		queryNode.setPreference("type", 5);
		
		String expectedToString = "DataNode [distance=close, color=red, price=$, type=car]-preferences={distance=1, color=1, price=1, type=5}";
		String actualToString = queryNode.toString();
		
		assertEquals(expectedToString, actualToString);
	}

	private void assertPreferencesLineUp(Map<String, Integer> expectedPreferences, Map<String, Integer> actualPreferences) {
		for (String preference: expectedPreferences.keySet()) {
			assertTrue(actualPreferences.containsKey(preference));
			assertEquals(expectedPreferences.get(preference), actualPreferences.get(preference));
		}
	}
	
	private void assertPreferenceWeightsLineUp(Map<String, Integer> expectedPreferences, QueryNode qn) {
		for (Entry<String, Integer> preferencePair: expectedPreferences.entrySet()) {
			int expectedPreference = preferencePair.getValue();
			int actualPreference = qn.getPreferenceWeightForAttribute(preferencePair.getKey());
			
			assertEquals(expectedPreference, actualPreference);
		}
	}
	
	private Map<String, Integer> getBottledPreferences() {
		Map<String, Integer> preferences = new HashMap<String, Integer>();
		preferences.put("price", 1);
		preferences.put("distance", 2);
		preferences.put("color", 1);
		preferences.put("type", 4);
		
		return preferences;
	}
	
	private Map<String, String> getBottledData() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("price", "$");
		data.put("distance", "close");
		data.put("color", "red");
		data.put("type", "car");
		
		return data;
	}
}
