package com.skyline.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.skyline.common.DataNode;
import com.skyline.common.DataReader;
import com.skyline.index.AttributeTree;

public class AttributeTreeTest {

	ArrayList<DataNode> dataNodes;
	DataReader dr;

	@Before
	public void setUp() throws Exception {
		dr = new DataReader("src/test/java/com/skyline/tests/data/test_data_1.txt");
		dataNodes = dr.generateDataNodes();
	}

	@Test
	public void testAdd() {
		AttributeTree at = new AttributeTree("price");
		
		for (DataNode dn: dataNodes)
			at.add(dn);
		
		assertEquals(2, at.size());
		assertEquals(1, at.getDataNodesForBranch("$$").size());
		assertEquals(2, at.getDataNodesForBranch("$$$").size());
		assertEquals(0, at.getDataNodesForBranch("a bad amount").size());
	}
	
	@Test
	public void testBulkAdd() {
		AttributeTree at = new AttributeTree("distance");
		at.add(dataNodes);
		
		assertEquals(2, at.size());
		assertEquals(1, at.getDataNodesForBranch("close").size());
		assertEquals(2, at.getDataNodesForBranch("far").size());
		assertEquals(0, at.getDataNodesForBranch("outter space").size());
	}
	
	@Test
	public void testGetBranchList() {
		AttributeTree at = new AttributeTree("price", dataNodes);
		
		ArrayList<DataNode> branchDataNodes = at.getDataNodesForBranch("$$$");
			
		for (DataNode dn: branchDataNodes)
			assertEquals("$$$", dn.getDataForAttribute("price"));
	}
	
	@Test
	public void testSearchTree() {
		AttributeTree at = new AttributeTree("price", dataNodes);
		
		ArrayList<DataNode> branchDataNodes = at.searchTree("$$$");

		for (DataNode dn: branchDataNodes)
			assertEquals("$$$", dn.getDataForAttribute("price"));
		
		
		assertEquals(2, branchDataNodes.size());
	} 
	
	@Test
	public void testToString() {
		AttributeTree at = new AttributeTree("price", dataNodes);
		
		String expectedToString = "price-tree contains 2 branches:\n"
				+ "\t\t$$-branch contains 1 nodes:\n"
				+ "\t\t\tDataNode [price=$$, distance=far]\n"
				+ "\t\t$$$-branch contains 2 nodes:\n"
				+ "\t\t\tDataNode [price=$$$, distance=close]\n"
				+ "\t\t\tDataNode [price=$$$, distance=far]\n";
		
		assertEquals(expectedToString, at.toString());
	}
}
